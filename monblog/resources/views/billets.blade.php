@extends('layouts.layout')

@section('page_contenu')
    
    @foreach($lesbillets as $billet)
        <h1 class="titreBillet"><a href="{{route('billets.show', $billet)}}">{{$billet->BIL_Titre}}</a></h1>
        <span>{{$billet->created_at}}<br/>
        {{$billet->BIL_Contenu}}</span>
    @endforeach
    
@endsection