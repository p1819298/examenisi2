<!Doctype HTML>
<html>
    <head>
        <title>Les billets du blog</title>
        <link rel="stylesheet" href="@yield('correction_url')assets/css/monblog.css" />
    </head>
    <body>
        <div id="global">
            <header id="titreBlog">
                <h1>Tous les billets :</h1>
            </header>
            @yield('page_contenu')
            <footer id="piedBlog">
                <p>Monblog - copyright 3A Info - 2022</p>
            </footer>
        </div>
    </body>
</html>