@extends('layouts.layout')

@section('correction_url')../@endsection

@section('page_contenu')
    <h2 class="titreBillet">{{$lebillet->BIL_Titre}}</h2>
    <p style="margin-top:0px;">{{$lebillet->created_at}}</p>
    <p>{{$lebillet->BIL_Contenu}}</p>

    <hr/>

    <h3>Réponses à {{$lebillet->BIL_Titre}}</h3>
    <p>
    @foreach($lescommentaires as $commentaire)
        <p>{{$commentaire->COM_Auteur}} dit :</p>
        <p>{{$commentaire->COM_Contenu}}</p>
    @endforeach
    </p>
@endsection