<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commentaires', function (Blueprint $table) {
            $table->date('COM_Date');
            $table->text('COM_Auteur');
            $table->text('COM_Contenu');
            $table->unsignedBigInteger('billet_id')
                ->references('id')
                ->on('billets')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
            $table->id();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commentaires');
    }
}
